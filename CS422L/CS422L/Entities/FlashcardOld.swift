//
//  Flashcard.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation

class FlashcardOld {
    var term: String = ""
    var definition: String = ""
    
    static func getHardCodedCollection() -> [FlashcardOld]
    {
        var flashcards = [FlashcardOld]()
        for i in 1...10
        {
            let flashcard = FlashcardOld()
            flashcard.term = "Term \(i)"
            flashcard.definition = "Definition \(i)"
            flashcards.append(flashcard)
        }
        return flashcards
    }
}
