//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Caleb Harrison on 2/16/21.
//

import UIKit

class StudySetViewController: UIViewController {

    var cards: [Flashcard] = [Flashcard]()
    
    @IBOutlet var flashcardLabel: UILabel!
    @IBOutlet var cardView: UIView!
    var selectedSet: FlashcardSet!
    
    var numberOfCorrect: Int = 0
    var numberOfIncorrect: Int = 0
    var numberOfCompleted: Int = 0
    
    @IBOutlet var correctLabel: UILabel!
    @IBOutlet var incorrectLabel: UILabel!
    @IBOutlet var completedCountLabel: UILabel!
    
    var currentIndex: Int = 0
    
    @IBOutlet var knowButton: UIButton!
    @IBOutlet var dontKnowButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCard()
        
        // get selected set's cards
        if let set = selectedSet {
            title = "\(set.title ?? "Set") Quiz"
            cards = Array(set.flashcards as! Set<Flashcard>)
        }
        
        flashcardLabel.text = cards[currentIndex].term
        
        incorrectLabel.text = "Incorrect: \(numberOfIncorrect)"
        correctLabel.text = "Correct: \(numberOfCorrect)"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(StudySetViewController.flipCard))
        flashcardLabel.isUserInteractionEnabled = true
        flashcardLabel.addGestureRecognizer(tap)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(StudySetViewController.respondToSwipe))
        swipeUp.direction = .up
        cardView.addGestureRecognizer(swipeUp)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(StudySetViewController.respondToSwipe))
        swipeLeft.direction = .left
        cardView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(StudySetViewController.respondToSwipe))
        swipeRight.direction = .right
        cardView.addGestureRecognizer(swipeRight)
    }
    
    @IBAction func knowButtonClicked() {
        numberOfCorrect += 1
        numberOfCompleted += 1
        goToNextCard()
    }
    
    @IBAction func dontKnowButtonClicked() {
        numberOfIncorrect += 1
        skipCard()
    }
    
    @IBAction func skipButtonClicked() {
        skipCard()
    }
    
    
    @objc func flipCard(sender: UITapGestureRecognizer) {
        let flashcard = cards[currentIndex]
        
        // check if currently showing term
        if flashcardLabel.text == flashcard.term {
            flashcardLabel.text = flashcard.definition
        } else {
            flashcardLabel.text = flashcard.term
        }
    }
    
    @objc func respondToSwipe(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
                // swiped right for correct
                case .right:
                    numberOfCorrect += 1
                    goToNextCard()
                    print("Swiped right/correct")
                // swiped left for incorrect
                case .left:
                    numberOfIncorrect += 1
                    goToNextCard()
                    print("Swiped left/incorrect")
                // swiped up for skip
                case .up:
                    skipCard()
                    print("Swiped up/skip")
                default:
                    break
            }
        }
    }
    
    func skipCard() {
        let flashcard = cards[currentIndex]
        
        // add current card to end
        cards.append(flashcard)
        
        // remove current card
        cards.remove(at: currentIndex)
        
        // goto next card
        currentIndex -= 1
        goToNextCard()
    }
    
    func goToNextCard() {
        currentIndex += 1
        
        // have we went through all cards?
        if currentIndex >= cards.count {
            currentIndex = 0
            numberOfCorrect = 0
            numberOfIncorrect = 0
            numberOfCompleted = 0
            //cards = Flashcard.getHardCodedCollection()
        }
        
        // update labels
        incorrectLabel.text = "Incorrect: \(numberOfIncorrect)"
        correctLabel.text = "Correct: \(numberOfCorrect)"
        completedCountLabel.text = "\(numberOfCompleted)/\(cards.count)"
        flashcardLabel.text = cards[currentIndex].term
    }
    
    func setupCard() {
        cardView.layer.cornerRadius = 8.0
        // add shadow to card
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOpacity = 1
        cardView.layer.shadowOffset = .zero
        cardView.layer.shadowRadius = 10
    }

}
