//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSet] = []
    var selectedIndex: Int = 0
    var setSelected: FlashcardSet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFlashcardSets()
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
        
        // long press to edit
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.7
        self.collectionView.addGestureRecognizer(longPressGesture)
    }
    
    func getFlashcardSets() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
         
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest =
           NSFetchRequest<NSManagedObject>(entityName: "FlashcardSet")
         
        do {
            sets = try managedContext.fetch(fetchRequest) as? [FlashcardSet] ?? []
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func addNewSet() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext =
        appDelegate.persistentContainer.viewContext

        let entity =
        NSEntityDescription.entity(forEntityName: "FlashcardSet",
                                  in: managedContext)!

        let set = NSManagedObject(entity: entity,
                                  insertInto: managedContext) as! FlashcardSet
        
        set.title = "Set \(sets.count + 1)"
        
        do {
            try managedContext.save()
            sets.append(set)
            collectionView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //handle long press function
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        //find out where the long press is
        let p = longPressGesture.location(in: self.collectionView)
        let indexPath = self.collectionView.indexPathForItem(at: p)
        let setTitle = sets[indexPath!.row].title
        if longPressGesture.state == UIGestureRecognizer.State.began {
            let alert = UIAlertController(title: "Would you like to delete \(setTitle ?? "this set")?", message: nil, preferredStyle: .alert)
            selectedIndex = indexPath!.row
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: {_ in
                alert.dismiss(animated: true, completion: {})
                self.deleteSet(indexPath: indexPath!)
            }))
            self.present(alert, animated: true)
        }
    }
    
    func deleteSet(indexPath: IndexPath) {
        let deletedSet = sets[selectedIndex]
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        managedContext.delete(deletedSet)
        sets.remove(at: selectedIndex)
        collectionView.deleteItems(at: [indexPath])
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // go to new view
        self.setSelected = self.sets[indexPath.row]
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    // another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetail"
        {
            if let vc = segue.destination as? FlashCardSetDetailViewController
            {
                vc.selectedSet = setSelected
            }
        }
    }
    
    
}

