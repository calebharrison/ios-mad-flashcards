//
//  AlertViewController.swift
//  CS422L
//
//  Created by Caleb Harrison on 2/16/21.
//

import UIKit
import CoreData

class AlertViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup view
        setupView()
        
        // set initial text
        flashcardTitle.text = parentVC.cards[parentVC.selectedIndex].term
        flashcardDefinition.text = parentVC.cards[parentVC.selectedIndex].definition
    }
    
    // parent view
    var parentVC: FlashCardSetDetailViewController!
    
    // alert view
    @IBOutlet var alertView: UIView!
    
    // flashcard title textfield
    @IBOutlet var flashcardTitle: UITextField!
    
    // flashcard definition textfield
    @IBOutlet var flashcardDefinition: UITextField!
    
    // save button function
    @IBAction func saveButtonClicked() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext =
        appDelegate.persistentContainer.viewContext
        
        
        do {
            // save changes
            parentVC.cards[parentVC.selectedIndex].term = flashcardTitle.text ?? ""
            parentVC.cards[parentVC.selectedIndex].definition = flashcardDefinition.text ?? ""
            try managedContext.save()
            parentVC.tableView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        self.dismiss(animated: true, completion: {})
    }
    
    // delete card function
    @IBAction func deleteButtonClicked() {
        let deletedCard = parentVC.cards[parentVC.selectedIndex]
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //delete the card and tell the tableview to reload
        
        parentVC.selectedSet?.removeFromFlashcards(deletedCard)
        managedContext.delete(deletedCard)
        parentVC.cards.remove(at: parentVC.selectedIndex)
        parentVC.tableView.reloadData()
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        self.dismiss(animated: true, completion: {})
    }
    
    // alert view UI setup
    func setupView() {
        alertView.layer.cornerRadius = 8.0
        alertView.layer.borderWidth = 2.0
        alertView.layer.borderColor = UIColor.gray.cgColor
        flashcardTitle.becomeFirstResponder()
    }
    
}
