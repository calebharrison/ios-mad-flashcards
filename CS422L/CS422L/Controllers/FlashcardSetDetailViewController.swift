//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit
import CoreData

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    public var cards: [Flashcard] = [Flashcard]()
    @IBOutlet var setTitleLabel: UILabel!
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    var selectedIndex: Int = 0
    var selectedSet: FlashcardSet!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        // get selected set's cards
        if let set = selectedSet
        {
            setTitleLabel.text = set.title
            cards = Array(set.flashcards as! Set<Flashcard>)
        }
        
        // long press to edit
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.7
        self.tableView.addGestureRecognizer(longPressGesture)
        
        makeItPretty()
    }
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        let managedContext =
        appDelegate.persistentContainer.viewContext

        let entity =
        NSEntityDescription.entity(forEntityName: "Flashcard",
                                  in: managedContext)!

        let card = NSManagedObject(entity: entity,
                                  insertInto: managedContext) as! Flashcard
        card.flashcardSet = selectedSet
        card.term = "New Card Term"
        card.definition = "New Card Definition"
        
        do {
            try managedContext.save()
            selectedSet.addToFlashcards(card)
            cards = Array(selectedSet?.flashcards as! Set<Flashcard>)
            tableView.reloadData()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //handle long press function
    @objc func handleLongPress(longPressGesture: UILongPressGestureRecognizer) {
        //find out where the long press is
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        //if long press is starting (this will also trigger on ending if you dont have this if statement)
        if longPressGesture.state == UIGestureRecognizer.State.began {
            selectedIndex = indexPath?.row ?? 0
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let alertVC = sb.instantiateViewController(identifier: "AlertViewController") as! AlertViewController
            alertVC.parentVC = self
            alertVC.modalPresentationStyle = .overCurrentContext
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteButtonClicked() {
        // go to main view controller
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func studyButtonClicked() {
        if cards.count > 0 {
            performSegue(withIdentifier: "GoToQuiz", sender: self)
        }
    }
    
    // create normal alert
    func createAlert(indexPath: IndexPath) {
        let alert = UIAlertController(title: cards[indexPath.row].term, message: cards[indexPath.row].definition, preferredStyle: .alert)
        selectedIndex = indexPath.row
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            alert.dismiss(animated: true, completion: {})
            self.createCustomEditAlert(index: indexPath.row)
        }))
        self.present(alert, animated: true)
    }
    
    // create custom alert
    func createCustomEditAlert(index: Int) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "AlertViewController") as! AlertViewController
        alertVC.parentVC = self
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: true, completion: nil)
    }
    
    // clicked card function
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        createAlert(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards[indexPath.row].term
        return cell
    }
    
    //just a function to make everything look nice
    func makeItPretty() {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToQuiz"
        {
            if let vc = segue.destination as? StudySetViewController
            {
                vc.selectedSet = selectedSet
            }
        }
    }
    
}
